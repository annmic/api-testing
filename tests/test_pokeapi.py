import requests


def test_default_list_of_pokemons():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    response_body = response.json()

    assert len(response_body["results"]) > 0
    assert response_body["results"]
    # assert "results" in response_body -> check only if list exists

    assert response.status_code == 200

    assert response_body["count"] == 1279

    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100
